#!/usr/bin/env python
# -*- coding: utf-8 -*-

#==================================
#========== TESTING FILE ==========
#==================================

#This file is meant as testing grounds for analysis and debugging of code written in other files in order to preserve readability of other files.

#========== INTERFACED SOLVER ==========
import SAT_Solver
from Solver_Definitions import *
from timeit import default_timer as timer
print("SAT solver testing:")

problem1 = SAT_Solver.read_dimacs_file("test1.cnf")
problem2 = SAT_Solver.read_dimacs_file("test2.cnf")


repeat = 1
tb = 0
tp = 0
for i in range(repeat):

    t1 = timer()
    solve_CNF(problem1, guess_key="frequent")
    solve_CNF(problem2, guess_key="frequent")
    t2 = timer()
    tb += t2-t1
    print("Finished test " + str(i*2 + 1) + "/" + str(repeat*2))

    t3 = timer()
    s1 = solve_CNF(problem1, pure=True, guess_key="frequent")
    s2 = solve_CNF(problem2, pure=True, guess_key="frequent")
    t4 = timer()
    tp += t4-t3
    print("Finished test " + str(i*2 + 2) + "/" + str(repeat*2))

    print(test_solution(problem1,s1))
    print(test_solution(problem2,s2))

print("Base: " + str(tb/repeat) + "\n" +"Pure: " + str(tp/repeat))






