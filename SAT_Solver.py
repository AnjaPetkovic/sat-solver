#!/usr/bin/env python
# -*- coding: utf-8 -*-

#================================
#========== SAT SOLVER ==========
#================================

from Solver_Definitions import *

def solve(input, output="", output_save=True, guess_key="random", pure=False):
    """
    Solves SAT problems in dimacs format. Unless specified otherwise, also saves the solution to a file.

    Input:          input;          name of a .txt file containing a SAT problem in dimacs format
                    output;         name of a .txt file where the solution should be saved. If left empty, a new file is generated.
                    output_save;    if set to False, the program will not save the solution in a file
                    guess_key;      determines the guessing choice
                    pure;           if set to True, the program will use searching for pure variables
    Output:         list of variable values that satisfy the SAT problem
    """

    #Read problem
    problem = read_dimacs_file(input)
    #Find solution
    solution = solve_CNF(problem, guess_key, pure)
    #Return solution
    if output_save:
        if output == "":
           output = input.split(".")[0] + "_solution.txt"
        write_dimacs_file(output, solution)
    return solution

def read_dimacs_file(input):
    """
    Reads files containing a dimacs format SAT problem and converting it to a format used by the SAT solver

    Input:          input;          name of a .txt file containing a SAT problem in dimacs format
    Output:         logical formula in a format used by the SAT solver
    """

    disjunction_list = []
    number_of_variables = 0
    number_of_clauses = 0
    temp_set = set()


    with open(input, "r") as file:
        for line in file:
            line = line.strip()
            #reading comments
            if line[0] == "c":
                pass
            #reading data line
            elif line[0] == "p":
                contents = line.split(" ")
                number_of_variables = int(contents[2])
                number_of_clauses = int(contents[3])
                if number_of_clauses > 10000:
                    print("Go grab a coffe, this may take a while ;)")
            #reading clauses
            else:
                contents = line.split(" ")
                for variable in contents:
                    if variable != "0":
                        temp_set.add(int(variable))
                    else:
                        disjunction_list.append(temp_set)
                        temp_set = set()
    #returning conjunction
    return disjunction_list

def write_dimacs_file(output, solution):
    """
    Writes the solution of the SAT solver to the output file.

    Input:          output;         name of a .txt file where the solution should be saved. If left empty, a new file is generated.
                    solution;       distionary of variable values
    Output:         None
    """

    if solution is None:
        output_string = "Not satisfiable."
    else:
        output_string = ""
        for var_name in solution:
            output_string = output_string + str(var_name) + " "
        output_string = output_string.strip()

    with open(output, "w") as file:
        file.write(output_string)
    return

