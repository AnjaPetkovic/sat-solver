import math

def graph_coloring(graph,k=3):
    """
    A function that converts the k-colorig of a graph question to a cnf fromula
    Input:          graph; = (V,E), V = a list of vertices, E = a list of tuples (v1,v2), that are edges
                    k;     number of colors
    Output:         n;number of variables
                    dijunction_list; a list of sets, that represent disjunctions, negations are denoted by minus variable
    """
    V,E = graph
    disjunction_list = [] #initializing the output list
    #for every color there is a sublist of variables
    color_dict = {} 
    nVariables = 1
    #create k variables for every vertex. Arrange variables into dictionary according to their vertices
    for vertex in V:
        color_dict[vertex] = [nVariables + i for i in range(k)]
        nVariables += k
    #begin writing formulas
    for vertex in color_dict.keys():
        disjunction = set()
        # every vertex is at least one color (red_vertex or blue_vertex or green_vertex)
        for color in range(k):
            disjunction.add(color_dict[vertex][color])
        disjunction_list.append(disjunction)
        #every vertex is at most one color (not red_vertex or not blue_vertex...)
        for color1 in range(k):
            for color2 in range(color1):
                if color1 != color2: #if clause just in case we don't mess up
                    var1 = color_dict[vertex][color1]
                    var2 = color_dict[vertex][color2]
                    disjunction_list.append({-var1,-var2})
    #disjunctions from edges
    for (v1,v2) in E:
        for color in range(k):
            var1 = color_dict[v1][color]
            var2 = color_dict[v2][color]
            disjunction_list.append({-var1,-var2})
    return (nVariables-1, disjunction_list)

def write_DIMACS(n,disjunction_list,output,comment=""):
    """
    A function, that creates a DIMACS format file from a CNF
    Input:          n; a number of variables in the dijsunction
                    disjunction_list; a list (conjunction to be) of sets (dijsunctions to be) of variables (numbers)
                    output; the name of the output file, where we write the DIMACS format
                    comment; a comment to be added at the begining of the format. It needs to be in one line.
    Output:         None;
    """
    with open(output,'w') as f:
        safe_comment = comment.split("\n")[0]
        print('c ' + safe_comment,file=f)
        print('p cnf {0} {1}'.format(n,len(disjunction_list)),file=f)
        for clause in disjunction_list:
            out_string = ''
            for var in clause:
                out_string = out_string + str(var) + ' '
            out_string = out_string + '0'
            print(out_string,file=f)



def sudoku_generate_CNF(n,sudoku,clauses="all"):
    """
    Generate the CNF formula form a sudoku
    Input:                  n;    sudoku dimension n x n
                       sudoku;    a list that represents sparse matrix with given sudoku numbers e.g. [(1,5,9)] means there is 9 in row 1 col 5
                       clauses;   a parameter that says which clauses we use. Possible values: all, sq1, sq2
    Output:              nVar;    number of variables
             disjunction_list;    a list of sets
    """
    #for every x,y,m: 0...n-1 we have a variable (x,y,m)
    var_dict = {}
    nVar = 1
    for x in range(n):
        for y in range(n):
            for m in range(n):
                var_dict[(x,y,m)] = nVar
                nVar +=1
    #we generate the clauses:
    #every square contains at least one number:
    sq1 = []
    for x in range(n):
        for y in range(n):
            disjunction = set()
            for m in range(n):
                disjunction.add(var_dict[(x,y,m)])
            sq1.append(disjunction)
    #at most one number in any square
    sq2 = []
    for x in range(n):
        for y in range(n):
            for m1 in range(n):
                for m2 in range(m1):
                    var1 = var_dict[(x,y,m1)]
                    var2 = var_dict[(x,y,m2)]
                    sq2.append({-var1,-var2})
    #every number appears in each row
    row1 = []
    for x in range(n):
        for m in range(n):
            disjunction = set()
            for y in range(n):
                disjunction.add(var_dict[(x,y,m)])
            row1.append(disjunction)
    #no number twice in the same row
    row2 = []
    for x in range(n):
        for m in range(n):
            for y1 in range(n):
                for y2 in range(m1):
                    var1 = var_dict[(x,y1,m)]
                    var2 = var_dict[(x,y2,m)]
                    row2.append({-var1,-var2})
    #every number appears in each column
    col1 = []
    for y in range(n):
        for m in range(n):
            disjunction = set()
            for x in range(n):
                disjunction.add(var_dict[(x,y,m)])
            row1.append(disjunction)
    #no number twice in the same column
    col2 = []
    for y in range(n):
        for m in range(n):
            for x1 in range(n):
                for x2 in range(m1):
                    var1 = var_dict[(x1,y,m)]
                    var2 = var_dict[(x2,y,m)]
                    row2.append({-var1,-var2})
    #every number appears in each minisquare
    mini1 = []
    root = int(math.sqrt(n))
    for i in range(root):
        for j in range(root):
            for m in range(n):
                disjunction = set()
                for x in range(i*root,(i+1)*root):
                    for y in range(j*root,(j+1)*root):
                        disjunction.add(var_dict[(x,y,m)])
                mini1.append(disjunction)
    #no number twice in the same miniqruare
    mini2 = []
    for i in range(root):
        for j in range(root):
            for m in range(n):
                for x1 in range(i*root,(i+1)*root):
                    for y1 in range(j*root,(j+1)*root):
                        for x2 in range(i*root,(i+1)*root):
                            for y2 in range(j*root,(j+1)*root):
                                if (x1,y1) != (x2,y2):
                                    var1 = var_dict[(x1,y1,m)]
                                    var2 = var_dict[(x2,y2,m)]
                                    mini2.append({-var1,-var2})
    #unit clauses
    units = []
    for x1,y1,m1 in sudoku:
        x = x1-1
        y = y1-1
        m = m1-1
        var = var_dict[(x,y,m)]
        units.append({var})
    if clauses == "sq1":
        disjunction_list = sq1 + row2 + col2 + mini2 + units
    elif clauses == 'sq2':
        disjunction_list = sq2 + row1 + col1 + mini1 + units
    else:
        disjunction_list = sq1 + row2 + col2 + mini2 + sq2 + row1 + col1 + mini1 + units
    return nVar-1, disjunction_list




    
    
            
                
    
        
            
        
        
            
    
